const express = require('express');
const mySqlDb = require("../mySqlDb");

const router = express.Router();

router.get('/', async (req, res) => {
  const [comments] = await mySqlDb.getConnection().query('SELECT id, news_id, author, comment FROM ??', ['comments']);
  res.send(comments);
})


router.post('/', async (req, res) => {
  if (!req.body.news_id || !req.body.comment) {
    return res.status(400).send({error: 'Data not valid'});
  }

  // const [isId] = await mySqlDb.getConnection().query(
  //   'SELECT * from ?? FIND_IN_SET(req.body.news_id,"id")',['news']);


  const comment = {
    news_id: req.body.news_id,
    author: req.body.author,
    comment: req.body.comment,
  };

    const [newComment] = await mySqlDb.getConnection().query(
      'INSERT INTO ?? (news_id, author, comment) values (?, ?,?)',
      ['comments', comment.news_id, comment.author, comment.comment]
    );

    res.send({
      ...comment,
      id: newComment.insertId
    });
});

router.delete('/:id', async (req, res) => {
  const [comment] = await mySqlDb.getConnection().query(
    `DELETE FROM ?? where id = ?`,
    ['comments', req.params.id])
  if (!comment) {
    return res.status(404).send({error: 'Data not found'});
  } else {
    res.send('Comment successfully has been deleted');
  }
});

module.exports = router; // export default router;