const express = require('express');
const multer = require("multer");
const {nanoid} = require("nanoid");
const mySqlDb = require("../mySqlDb");
const path = require('path');
const config = require('../config');


const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

const router = express.Router();

router.get('/', async (req, res) => {
  const [news] = await mySqlDb.getConnection().query('SELECT id, title, image, publish_date FROM ??', ['news']);
  res.send(news);
})

router.get('/:id', async (req, res) => {
  const [one_news] = await mySqlDb.getConnection().query(
    `SELECT * FROM ?? where id = ?`,
    ['news', req.params.id])
  if (!one_news) {
    return res.status(404).send({error: 'Data not found'});
  }

  res.send(one_news[0]);
});

router.post('/', upload.single('image'), async (req, res) => {
  if (!req.body.title || !req.body.content) {
    return res.status(400).send({error: 'Data not valid'});
  }
//
  const date = new Date();
  // const currentDate = date.toISOString();

  const postNews = {
    title: req.body.title,
    content: req.body.content,
    publish_date: date,
  };

  if (req.file) {
    postNews.image = req.file.filename;
  }

  const newPostNews = await mySqlDb.getConnection().query(
    'INSERT INTO ?? (title, content, image, publish_date) values (?, ?, ?,?)',
    ['news', postNews.title, postNews.content, postNews.image, postNews.publish_date]
  );

  res.send({
    ...postNews,
    id: newPostNews[0].insertId
  });

 });

router.delete('/:id', async (req, res) => {
  const [one_news] = await mySqlDb.getConnection().query(
    `DELETE FROM ?? where id = ?`,
    ['news', req.params.id])
  if (!one_news) {
    return res.status(404).send({error: 'Data not found'});
  } else {
    res.send('News successfully has been deleted');
  }
});


module.exports = router; // export default router;