import React from 'react';
import './PostShow.css';
import {NavLink} from "react-router-dom";

const PostShow = props => {
    return (
        <div className='post-box' onClick={props.onClick}>
            {/*<img src={props.image} alt="Contact"/>*/}
            <h3>{props.title}</h3>
            <p>{props.publish_date}</p>
            <NavLink to='/fullPost'>Read more</NavLink>
        </div>
    );
};

export default PostShow;