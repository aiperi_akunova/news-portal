import React, {useEffect} from 'react';
import {NavLink} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {fetchNews} from "../../store/actions/newsActions";
import PostShow from "../PostShow/PostShow";

const Posts = () => {
    const dispatch = useDispatch();
    const posts = useSelector(state => state.posts);

    useEffect(() => {
        dispatch(fetchNews());
    }, [dispatch]);
    let allPosts = null;
    if (posts) {
        allPosts = posts.map(post => (
            <PostShow
                key={post.id}
                title={post.title}
                content={post.content}
                image={post.image}
                publish_date={post.publish_date}/>
        ))
    }

    return (
        <div>
            <h1>Posts</h1>
            <NavLink to='/newPost' className='add-btn'>Add new post</NavLink>
            <div className='posts'>
                {allPosts}
            </div>
        </div>
    );
};

export default Posts;