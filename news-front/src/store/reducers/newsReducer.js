import {
  FETCH_NEWS_FAILURE,
  FETCH_NEWS_REQUEST,
  FETCH_NEWS_SUCCESS,
  FETCH_POST_FAILURE,
  FETCH_POST_REQUEST,
  FETCH_POST_SUCCESS
} from "../actions/newsActions";

const initialState = {
  fetchLoading: false,
  singleLoading: false,
  posts: [],
  post: null,
  comments:[],
};

const newsReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_NEWS_REQUEST:
      return {...state, fetchLoading: true};
    case FETCH_NEWS_SUCCESS:
      return {...state, fetchLoading: false, posts: action.payload};
    case FETCH_NEWS_FAILURE:
      return {...state, fetchLoading: false};
    case FETCH_POST_REQUEST:
      return {...state, singleLoading: true};
    case FETCH_POST_SUCCESS:
      return {...state, singleLoading: false, post: action.payload};
    case FETCH_POST_FAILURE:
      return {...state, singleLoading: false};
    default:
      return state;
  }
};


export default newsReducer;