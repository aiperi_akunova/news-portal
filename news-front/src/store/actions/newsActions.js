import axios from "axios";

export const FETCH_NEWS_REQUEST = 'FETCH_NEWS_REQUEST';
export const FETCH_NEWS_SUCCESS = 'FETCH_NEWS_SUCCESS';
export const FETCH_NEWS_FAILURE = 'FETCH_NEWS_FAILURE';

export const CREATE_NEWS_REQUEST = 'CREATE_NEWS_REQUEST';
export const CREATE_NEWS_SUCCESS = 'CREATE_NEWS_SUCCESS';
export const CREATE_NEWS_FAILURE = 'CREATE_NEWS_FAILURE';

export const FETCH_POST_REQUEST = 'FETCH_POST_REQUEST';
export const FETCH_POST_SUCCESS = 'FETCH_POST_SUCCESS';
export const FETCH_POST_FAILURE = 'FETCH_POST_FAILURE';

export const fetchPostRequest = () => ({type: FETCH_POST_REQUEST});
export const fetchPostSuccess = post => ({type: FETCH_POST_SUCCESS, payload: post});
export const fetchPostFailure = () => ({type: FETCH_POST_FAILURE});

export const fetchNewsRequest = () => ({type: FETCH_NEWS_REQUEST});
export const fetchNewsSuccess = posts => ({type: FETCH_NEWS_SUCCESS, payload: posts});
export const fetchNewsFailure = () => ({type: FETCH_NEWS_FAILURE});

export const createNewsRequest = () => ({type: CREATE_NEWS_REQUEST});
export const createNewsSuccess = () => ({type: CREATE_NEWS_SUCCESS});
export const createNewsFailure = () => ({type: CREATE_NEWS_FAILURE});


export const fetchNews = () => {
  return async dispatch => {
    try {
      dispatch(fetchNewsRequest());
      const response = await axios.get('http://localhost:8000/news');
      dispatch(fetchNewsSuccess(response.data));
    } catch (e) {
      dispatch(fetchNewsFailure());
    }
  };
};

export const fetchPost = id => {
  return async dispatch => {
    try {
      dispatch(fetchPostRequest());
      const response = await axios.get('http://localhost:8000/news/' + id);
      dispatch(fetchPostSuccess(response.data));
    } catch (e) {
      dispatch(fetchPostFailure());
    }
  };
};


export const createNews = news => {
  return async dispatch => {
    try {
      dispatch(createNewsRequest());
      await axios.post('http://localhost:8000/news', news);
      dispatch(createNewsSuccess());
    } catch (e) {
      dispatch(createNewsFailure());
      throw e;
    }
  };
};


