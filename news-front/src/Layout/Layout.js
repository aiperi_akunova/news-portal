import React from 'react';
import './Layout.css';
import {NavLink} from "react-router-dom";

const Layout = ({children}) => {
    return (
        <div className='App'>
            <header className='header'>
               <NavLink to='/' className='logo'>News</NavLink>
            </header>
            <main className='Layout-box'>
                {children}
            </main>
            
        </div>
    );
};

export default Layout;