import React, {useState} from 'react';
import './AddPost.css';
import {useDispatch} from "react-redux";
import {createNews} from "../../store/actions/newsActions";

const AddPost = ({history}) => {

    const dispatch = useDispatch();

    const [post, setPost] = useState({
        title: '',
        content: '',
        image: null,
    });

    const onInputChange = e => {
        const {name, value} = e.target;

        setPost(prev => ({
            ...prev,
            [name]: value
        }));
    };
    const fileChangeHandler = e =>{
        const name = e.target.name;
        const file = e.target.files[0];
        setPost(prevState => {
            return {...prevState, [name]: file}
        })
    }

    const submitFormHandler = async e => {
        e.preventDefault();

        const formData = new FormData();
        formData.append('title', post.title);
        formData.append('content', post.content);
        formData.append('image', post.image);
       await dispatch(createNews(formData));
       history.replace('/');

    };

    return (
        <div>
           <h1>Add new post</h1>
            <form className='add-post-form'>
            <input
                type='text'
                name='title'
                value={post.title}
                onChange={onInputChange}
                autoComplete='off'
                placeholder='Title of the news'
            />
            <input
                type='text'
                name='content'
                value={post.content}
                onChange={onInputChange}
                autoComplete='off'
                placeholder='Content of the news'
            />
                <label htmlFor="image">Image:</label>
                <input
                    autoComplete='off'
                    type='file'
                    name='image'
                    onChange={fileChangeHandler}/>

            <button onClick={submitFormHandler}>Save</button>
            </form>
        </div>
    );
};

export default AddPost;