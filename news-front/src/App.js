import './App.css';
import {BrowserRouter, Route, Switch} from "react-router-dom";
import Layout from "./Layout/Layout";
import Posts from "./components/Posts/Posts";
import AddPost from "./containers/AddPost/AddPost";
import FullPost from "./components/FullPost/FullPost";


const App = () => {
  return (
      <BrowserRouter>
          <Switch>
              <Layout>
                  <Route path='/' exact component={Posts}/>
                  <Route path='/newPost' component={AddPost}/>
                  <Route path='/fullPost' component={FullPost}/>
              </Layout>
          </Switch>
      </BrowserRouter>
  );
};

export default App;
