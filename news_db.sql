drop database if exists newsDb;
create database if not exists newsDb;
use newsDb;

create table if not exists news (
    id int not null auto_increment primary key,
    title varchar(255) not null,
    content text not null,
    image varchar(255) null,
    publish_date datetime null
);

create table if not exists comments
(
    id      int          not null auto_increment primary key,
    news_id int          not null,
    author  varchar(255) null,
    comment text         not null,
    constraint comments_news_id_fk
        foreign key (news_id)
            references news (id)
            on delete cascade
);
insert into news (title, content, image, publish_date) values
 ('Breaking Law', 'someone somewhere did something', null, null),
 ('Kidnapping in Bishkek', 'someone kidnapped', null, null),
 ('Elections', 'people are going to choose', null, null);

